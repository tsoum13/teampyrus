﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HeadlinesApp.Models
{
    public class HeadlineModel
    {
        public string Headline { get; set; }
        public DateTime TimeStamp { get; set; }
        public int likes;
        public int id;

        public HeadlineModel(string Headline, int likes, int id)
        {
            this.Headline = Headline;
            this.TimeStamp = DateTime.Now;
            this.likes = likes;
            this.id = id;
        }

        public HeadlineModel(string Headline, int likes, DateTime TimeStamp, int id)
        {
            this.Headline = Headline;
            this.TimeStamp = TimeStamp;
            this.likes = likes;
            this.id = id;
        }

        

    }
}