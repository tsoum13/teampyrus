﻿CREATE DATABASE [HeadlinesAppDb] 
 ON PRIMARY
 (NAME = N'HeadlinesApp', FILENAME= N'C:\Users\TS\teampyrus\milestone3\HeadlinesApp\HeadlinesApp\App_Data\HeadlinesApp.mdf', SIZE = 8192KB, FILEGROWTH = 65536KB)
 LOG ON
 (NAME = N'HeadlinesApp_Log', FILENAME= N'C:\Users\TS\teampyrus\milestone3\HeadlinesApp\HeadlinesApp\App_Data\HeadlinesApp_log.ldf', SIZE = 8192KB, FILEGROWTH = 65536KB)
 GO
